from django.conf.urls.defaults import *
from django.contrib import admin

urlpatterns = patterns('',
    ('^intranet/(.*)', admin.site.root),
    (r'', include('accopensys.www.urls')),
)

