from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.utils.safestring import mark_safe
from models import Section, Page

def generic(request, section=None, page=None):
	if not section:
		section = Section.objects.all().order_by('position')
		if section:
			section = section[0]
		else:
			raise Http404
		return HttpResponseRedirect('/%s' % section.slug)
	else:
		section = Section.objects.filter(slug=section)
		if not section:
			raise Http404
		else:
			section = section[0]

	if not page:
		page = Page.objects.filter(section=section.id).order_by('position')
		if page:
			page = page[0]
		else:
			raise Http404
		return HttpResponseRedirect('/%s/%s' % (section.slug, page.slug))
	else:
		page = Page.objects.filter(section=section.id, slug=page)
		if not page:
			raise Http404
		else:
			page = page[0]

	sections = Section.objects.filter(visible=True).order_by('position')
	pages = Page.objects.filter(visible=True, section=section.id)

	rss = {'title': 'Accopensys RSS', 'url': 'http://www.accopensys.com/?rss'}

	return render_to_response('generic.html', {'request': request, 'sections': sections, 'pages': pages, 'object': page})

