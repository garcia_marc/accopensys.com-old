from django.conf.urls.defaults import *
from models import Section

urlpatterns = patterns('django.views.generic.simple',
	(r'^sign-in$', 'direct_to_template', {'template': 'login.html', 'extra_context': {'sections':  Section.objects.filter(visible=True).order_by('position')}}),
)
urlpatterns += patterns('accopensys.www.views',
	(r'^$', 'generic', {}),
	(r'^(?P<section>[a-z0-9\-]+)$', 'generic', {}),
	(r'^(?P<section>[a-z0-9\-]+)/(?P<page>[a-z0-9\-]+)$', 'generic', {}),
)

