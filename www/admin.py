from django.contrib import admin
from models import *

class Section_Options(admin.ModelAdmin):
	model = Section
	list_display = ['name', 'position', 'visible']
	list_filter = ['visible']
	search_fields = ['name', 'slug']

class Page_Options(admin.ModelAdmin):
	model = Page
	list_display = ['name', 'section', 'position', 'visible']
	list_filter = ['section', 'visible']
	search_fields = ['name', 'slug', 'content']

admin.site.register(Section, Section_Options)
admin.site.register(Page, Page_Options)

