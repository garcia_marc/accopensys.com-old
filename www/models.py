from django.db import models
from django.template.defaultfilters import slugify
from django.utils.safestring import mark_safe

class Section(models.Model):
	name = models.CharField(max_length=64)
	position = models.IntegerField()
	visible = models.BooleanField()
	slug = models.SlugField(editable=False, unique=True)

	class Meta:
		ordering = ['position']

	def __unicode__(self):
		return self.name

	def save(self):
		self.slug = slugify(self.name)
		super(Section, self).save()

class Page(models.Model):
	section = models.ForeignKey(Section)
	name = models.CharField(max_length=64)
	content = models.TextField()
	image = models.ImageField(upload_to="img/uploaded/page/image")
	position = models.IntegerField()
	visible = models.BooleanField()
	slug = models.SlugField(editable=False, unique=True)

	class Meta:
		ordering = ['section', 'position']

	def __unicode__(self):
		return self.name

	def save(self):
		self.slug = slugify(self.name)
		super(Page, self).save()

	def content_safe(self):
		return mark_safe(self.content)

import admin

