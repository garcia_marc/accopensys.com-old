# -*- coding: utf-8 -*-
import os

project = os.path.split(os.path.dirname(os.path.abspath(__file__)))[-1]

DEBUG = True
TEMPLATE_DEBUG = DEBUG
ADMINS = (
	('WebAdmin', 'webadmin@accopensys.com'),
)
MANAGERS = ADMINS
DATABASE_ENGINE = 'mysql'
DATABASE_NAME = 'accopensys'
DATABASE_USER = 'accopensys'
DATABASE_PASSWORD = 'tjacom1234'
DATABASE_HOST = 'localhost'
DATABASE_PORT = ''
TIME_ZONE = 'Europe/Andorra'
LANGUAGES = [
  ['en', u'English'],
  ['ca', u'Catal'],
  ['es', u'Espaol']
]
LANGUAGE_CODE = 'en'
USE_I18N = True
MEDIA_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'media/')
MEDIA_URL = '/'
ADMIN_MEDIA_PREFIX = '/intranet/'
SECRET_KEY = '7n2344_2ldvkxr=u(0=cu*npgil+v1*4q00g9qzzz!edh)1rlh'
CACHE_BACKEND = 'dummy:///'
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
)
TEMPLATE_LOADERS = (
    'django.template.loaders.app_directories.load_template_source',
)
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)
ROOT_URLCONF = '%s.urls' % project
TEMPLATE_DIRS = ()
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'transdb',
    '%s.www' % project,
    'django.contrib.admin',
)

